within ;
model PRLtestrev3

  Modelica.SIunits.Mass ref_mass;
  inner TIL.SystemInformationManager sim(
    redeclare TILMedia.VLEFluidTypes.TILMedia_R410APPF vleFluidType1,
    redeclare TILMedia.VLEFluidTypes.TILMedia_R410APPF vleFluidType2,
    redeclare TILMedia.GasTypes.TILMedia_MoistAir gasType1,
    redeclare TILMedia.LiquidTypes.TILMedia_Water liquidType1)
                                              annotation (Placement(transformation(extent={{212,192},
            {256,236}},       rotation=0)));
  TIL.LiquidComponents.Sensors.Sensor_m_flow sensor_m_flow1 annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=180,
        origin={-81,139})));
  TIL.LiquidComponents.Sensors.Sensor_T_degC sensor_T_degC5
    annotation (Placement(transformation(extent={{-120,138},{-108,150}})));
  TIL.LiquidComponents.Sensors.Sensor_T_degC sensor_T_degC6
    annotation (Placement(transformation(extent={{-172,138},{-160,150}})));
  TIL.LiquidComponents.Tubes.Tube tube(
    redeclare TIL.LiquidComponents.Tubes.Geometry.TubeGeometry tubeGeometry(
      innerDiameter(displayUnit="mm") = 0.0001524,
      wallThickness(displayUnit="mm") = 0.0039116,
      length=0.8128,
      crossSectionType=TIL.Internals.CrossSectionType.Circular),
    nCells=10,
    redeclare model TubeSideHeatTransferModel =
        TIL.LiquidComponents.Tubes.TransportPhenomena.HeatTransfer.ConstantAlpha
        (constantAlpha=2000),
    redeclare model WallHeatConductionModel =
        TIL.LiquidComponents.Tubes.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    redeclare model PressureDropModel =
        TIL.LiquidComponents.Tubes.TransportPhenomena.PressureDrop.ConstantPressureDrop
        (constantPressureDrop(displayUnit="kPa") = 30000),
    enableHeatPorts=true)
    annotation (Placement(transformation(extent={{-130,194},{-114,198}})));

  TIL.OtherComponents.Thermal.HeatBoundary Heater(
    boundaryType="Q_flow",
    use_heatFlowRateInput=true,
    use_temperatureInput=false,
    Q_flowFixed(displayUnit="kW") = -18380) annotation (Placement(
        transformation(
        extent={{-4,-6},{4,6}},
        rotation=-90,
        origin={-122,226})));
  TIL.LiquidComponents.Pumps.Pump2ndOrder Waterpump(
    use_mechanicalPort=false,
    n0=50,
    dp0(displayUnit="kPa") = 418200,
    V_flow0=0.00089,
    eta0=0.5,
    T0=293.15,
    nFixed=40) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={-70,170})));
  TIL.LiquidComponents.HydraulicCapacitors.HydraulicCapacitor
    hydraulicCapacitor(
    C=0.1/1e5,
    massInitial=0.1,
    TInitial=323.15,
    pInitial=100000,
    fixedInitialPressure=false) annotation (Placement(transformation(
        extent={{-4,-8},{4,8}},
        rotation=90,
        origin={-150,170})));
  TIL.HeatExchangers.Plate.VLEFluidLiquid.ParallelFlowHX Preevaporator(
    liquidType_b=sim.liquidType1,
    vleFluidType_a=sim.vleFluidType2,
    hxGeometry(
      numberOfPlates=54,
      length(displayUnit="mm") = 0.466,
      width(displayUnit="mm") = 0.05,
      phi=35,
      wallThickness(displayUnit="mm") = 0.00075,
      patternAmplitude(displayUnit="mm") = 0.002,
      patternWaveLength(displayUnit="mm") = 0.0126),
    nCells=10,
    pressureStateID=2,
    redeclare model HeatTransferModel_a =
        TIL.HeatExchangers.Plate.TransportPhenomena.HeatTransfer.ConstantAlpha
        (constantAlpha=30000),
    redeclare model PressureDropModel_a =
        TIL.HeatExchangers.Plate.TransportPhenomena.PressureDrop.QuadraticMassFlowDependent
        (mdot_nominal=0.25, pressureDrop_nominal(displayUnit="kPa") = 34500),
    redeclare model HeatTransferModel_b =
        TIL.HeatExchangers.Plate.TransportPhenomena.HeatTransfer.ConstantAlpha
        (constantAlpha=1800),
    redeclare model PressureDropModel_b =
        TIL.HeatExchangers.Plate.TransportPhenomena.PressureDrop.ZeroPressureDrop,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Steel,
    redeclare model WallHeatConductionModel =
        TIL.HeatExchangers.Plate.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    m_flowStart_a=1e-4,
    pStart_a=500000)
    annotation (Placement(transformation(extent={{-138,58},{-110,86}})));

  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{-204,86},{-184,106}})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC1(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{-48,88},{-28,108}})));
  TIL.LiquidComponents.Sensors.Sensor_p sensor_p
    annotation (Placement(transformation(extent={{-58,118},{-46,130}})));
  TIL.HeatExchangers.MPET.MoistAirVLEFluid.CrossFlowHX TestCoil(
    vleFluidType=sim.vleFluidType2,
    hInitialVLEFluid=350e3,
    pressureStateID=2,
    initVLEFluid="linearEnthalpyDistribution",
    nCellsPerPass=10,
    redeclare model WallHeatConductionModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    redeclare model FinSideHeatTransferModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.FinSideHeatTransfer.Chang,
    redeclare model FinSidePressureDropModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.FinSidePressureDrop.ZeroPressureDrop,
    pressureDropInitialVLEFluid=500,
    pressureDropInitialMoistAir=500,
    mWaterInitial=1e-3,
    wallCellStateType="state north",
    flagActivateDynWaterBalance=true,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Aluminum,
    redeclare model FinEfficiencyModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.FinEfficiency.ConstFinEfficiency,
    hInitialVLEFluid_Cell1=250e3,
    redeclare model TubeSidePressureDropModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.TubeSidePressureDrop.QuadraticMassFlowDependent
        (mdot_nominal=0.25, pressureDrop_nominal(displayUnit="kPa") = 70000),
    redeclare TIL.HeatExchangers.MPET.Geometry.Example hxGeometry(
      hxHeight(displayUnit="mm") = 1.3629,
      hxWidth(displayUnit="mm") = 1.274,
      hxDepth(displayUnit="mm") = 0.0254,
      nPasses=2,
      nTubesPerPass={33,32},
      nPortsPerTube=13,
      portDiameter=0.8e-3,
      tubeThickness=1.5e-3,
      finThickness=0.1e-3,
      finPitch(displayUnit="mm") = 0.0011,
      portCrossSectionalAreaNonCircular=3.5e-6,
      portPerimeterNonCircular=7e-3),
    m_flowVLEFluidStart=1e-4,
    m_flowMoistAirStart=0.15,
    redeclare model TubeSideHeatTransferModel =
        TIL.HeatExchangers.MPET.TransportPhenomena.TubeSideHeatTransfer.ConstantAlpha
        (constantAlpha=19000),
    hInitialVLEFluid_CellN=420e3,
    pVLEFluidStart=500000,
    TInitialWall=298.15,
    fixedPressureDropInitialMoistAir=true)
    annotation (Placement(transformation(extent={{96,50},{124,78}},
                                                                  rotation=0)));

  TIL.GasComponents.Boundaries.BoundaryOverdetermined
                                                  moistAirSource(
    pFixed=101300,
    use_massFlowRateInput=false,
    V_flowFixed=-0.4,
    boundaryType="p, m_flow",
    streamVariablesInputTypeConcentration="phi",
    phiFixed=10,
    m_flowFixed=-5.1,
    TFixed=292.52)                     annotation (Placement(transformation(
        origin={110,162},
        extent={{-6,-14},{6,14}},
        rotation=90)));
  TIL.GasComponents.Boundaries.BoundaryUnderdetermined
                                                 moistAirSink(
    streamVariablesInputType="T",
    streamVariablesInputTypeConcentration="phi",
    phiFixed=10,
    TFixed=287.15)
               annotation (Placement(transformation(
        origin={110,-8},
        extent={{-4,-10},{4,10}},
        rotation=90)));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC2(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{38,94},{58,114}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p1(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={8,102})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC3(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{150,90},{172,112}})));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow2(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{-22,56},{-6,72}})));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow3(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{218,54},{238,74}})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{78,94},{98,114}})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q1(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{188,88},{210,110}})));
  TIL.GasComponents.Sensors.Sensor_T_degC sensor_T_degC4
    annotation (Placement(transformation(extent={{146,18},{160,32}})));
  TIL.GasComponents.Sensors.Sensor_T_degC sensor_T_degC7
    annotation (Placement(transformation(extent={{138,136},{156,154}})));
  TIL.HeatExchangers.Plate.VLEFluidVLEFluid.ParallelFlowHX CascadeCondenser(
    redeclare TIL.HeatExchangers.Plate.Geometry.PlateGeometry hxGeometry(
      numberOfPlates=50,
      length(displayUnit="mm") = 0.52,
      width(displayUnit="mm") = 0.186,
      phi=90,
      wallThickness(displayUnit="mm") = 0.00075,
      patternAmplitude(displayUnit="mm") = 0.002,
      patternWaveLength(displayUnit="mm") = 0.0126),
    nCells=10,
    pressureStateID_a=1,
    vleFluidType_a=sim.vleFluidType1,
    pressureStateID_b=2,
    vleFluidType_b=sim.vleFluidType2,
    redeclare model HeatTransferModel_a =
        TIL.HeatExchangers.Plate.TransportPhenomena.HeatTransfer.ConstantAlpha
        (constantAlpha=2500),
    redeclare model PressureDropModel_a =
        TIL.HeatExchangers.Plate.TransportPhenomena.PressureDrop.ZeroPressureDrop,
    redeclare model VLEFluidCell_a = TIL.Cells.VLEFluidCell,
    steadyStateContinuity_a=false,
    redeclare model HeatTransferModel_b =
        TIL.HeatExchangers.Plate.TransportPhenomena.HeatTransfer.ConstantAlpha
        (constantAlpha=2500),
    redeclare model PressureDropModel_b =
        TIL.HeatExchangers.Plate.TransportPhenomena.PressureDrop.ZeroPressureDrop,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Steel,
    redeclare model WallHeatConductionModel =
        TIL.HeatExchangers.Plate.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    initVLEFluid_a="userSpecifiedValues")
    annotation (Placement(transformation(extent={{72,-134},{100,-106}})));

  TIL.VLEFluidComponents.Boundaries.BoundaryOverdetermined
    boundaryOverdetermined2(
    vleFluidType=sim.vleFluidType1,
    streamVariablesInputType="T",
    hFixed=237600,
    TFixed=270.07,
    mixingRatioFixed={1},
    boundaryType="p, m_flow",
    pFixed(displayUnit="kPa") = 724500,
    m_flowFixed=-0.2991)
    annotation (Placement(transformation(extent={{-132,-276},{-112,-224}})));
  TIL.VLEFluidComponents.Boundaries.BoundaryUnderdetermined
    boundaryUnderdetermined2(
    streamVariablesInputType="T",
    hFixed=432100,
    TFixed=281.25,
    mixingRatioFixed={1})
    annotation (Placement(transformation(extent={{208,-274},{226,-230}})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC8(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{100,-66},{120,-46}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p3(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-14,-58})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC9(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{48,-68},{68,-48}})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC10
    annotation (Placement(transformation(extent={{-14,-198},{6,-178}})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC11
    annotation (Placement(transformation(extent={{162,-190},{184,-168}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p4(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={188,-56})));
  TIL.VLEFluidComponents.PressureStateElements.PressureState pressureState4(
    vleFluidType=sim.vleFluidType2,
    pressureStateID=2,
    pInitial=997800)  annotation (Placement(transformation(
        origin={213,-112},
        extent={{15,10},{-15,-10}},
        rotation=0)));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow5(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-30,-112})));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow7(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(extent={{-12,-12},{12,12}},
        rotation=0,
        origin={156,-252})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q2(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(extent={{-14,-232},{6,-212}})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q3(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(extent={{160,-224},{182,-202}})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q4(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{10,-74},{30,-54}})));
  TIL.VLEFluidComponents.Sensors.Sensor_q sensor_q5(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{140,-66},{160,-46}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p5(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(
        extent={{-11,-11},{11,11}},
        rotation=0,
        origin={173,-143})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p6(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(
        extent={{-11,-11},{11,11}},
        rotation=0,
        origin={-5,-155})));
  TIL.VLEFluidComponents.Pumps.Pump2ndOrder RefrigerantPump(
    vleFluidType=sim.vleFluidType2,
    use_mechanicalPort=true,
    n0(displayUnit="Hz") = 35,
    dp0(displayUnit="kPa") = 385400,
    V_flow0=0.00092,
    eta0=0.93,
    p0(displayUnit="kPa") = 138900,
    T0=293.15,
    nFixed(displayUnit="Hz") = 8.255)
                                   annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-70,-112})));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow8(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-168,-112})));
  TIL.VLEFluidComponents.PressureStateElements.PressureState pressureState1(
      pressureStateID=1, pInitial(displayUnit="kPa") = 724500)
                               annotation (Placement(transformation(extent={{-10,
            -260},{6,-240}},  rotation=0)));
  TIL.VLEFluidComponents.Sensors.Sensor_m_flow sensor_m_flow4(vleFluidType=sim.vleFluidType1)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-68,-250})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_p7(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={-139,-59})));
  TIL.VLEFluidComponents.Sensors.Sensor_T_degC sensor_T_degC12(vleFluidType=sim.vleFluidType2)
    annotation (Placement(transformation(extent={{-106,-70},{-86,-50}})));
  TIL.VLEFluidComponents.FillingStations.FillingStation fillingStation(
    vleFluidType=sim.vleFluidType2,
    totalMassFixed=3.55,
    maxFillingMassFlowRate=0.01) annotation (Placement(transformation(
        extent={{-17,-20},{17,20}},
        rotation=90,
        origin={-198,-19})));
  TIL.OtherComponents.Controllers.PIController
                                           PI_m_supply(
    yMin=0.1,
    yMax=30,
    k=2,
    Ti=0.001,
    controllerType="PI",
    initType="initialOutput",
    yInitial=7)      annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={-106,-168})));
  TIL.OtherComponents.Mechanical.RotatoryBoundary rotatoryBoundary(use_nInput=
        true)
    annotation (Placement(transformation(extent={{-108,-154},{-100,-134}})));
  Modelica.Blocks.Sources.RealExpression set_m_supply(y=0.25) annotation (
      Placement(transformation(
        origin={-68,-168},
        extent={{-10,-10},{10,10}},
        rotation=180)));
  TIL.OtherComponents.Controllers.PIController PI_q_supply(
    invertFeedback=true,
    yMin=-40000,
    yMax=40000,
    k=2,
    Ti=0.01,
    controllerType="PI",
    initType="initialOutput",
    yInitial=-18000) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={38,182})));
  Modelica.Blocks.Sources.Constant setPoint_quality(k=0.3) annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=0,
        origin={74,180})));
  Modelica.Blocks.Math.Min min
    annotation (Placement(transformation(extent={{-12,200},{8,220}})));
  Modelica.Blocks.Math.Gain gain(k=-73000)
    annotation (Placement(transformation(extent={{-26,138},{-16,148}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{0,154},{12,166}})));
  Modelica.Blocks.Sources.Constant initial_heat(k=0) annotation (Placement(
        transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={-52,216})));
equation
  connect(Preevaporator.portB_b,sensor_m_flow1. portB) annotation (Line(
      points={{-110,80},{-102.125,80},{-102.125,139},{-86.25,139}},
      color={0,170,238},
      thickness=0.5));
  connect(sensor_T_degC5.port,sensor_m_flow1. portB) annotation (Line(
      points={{-114,138},{-102.125,138},{-102.125,139},{-86.25,139}},
      color={0,170,238},
      thickness=0.5));
  connect(sensor_T_degC6.port,Preevaporator. portA_b) annotation (Line(
      points={{-166,138},{-146,138},{-146,80},{-138,80}},
      color={0,170,238},
      thickness=0.5));
  connect(Heater.heatPort, tube.heatPort[1]) annotation (Line(
      points={{-122,226},{-122,197.1}},
      color={204,0,0},
      thickness=0.5));
  connect(Waterpump.portB, sensor_m_flow1.portA) annotation (Line(
      points={{-70,164},{-70,139},{-75.75,139}},
      color={0,170,238},
      thickness=0.5));
  connect(tube.portB, Waterpump.portA) annotation (Line(
      points={{-114,196},{-70,196},{-70,176}},
      color={0,170,238},
      thickness=0.5));
  connect(tube.portA,hydraulicCapacitor. portB) annotation (Line(
      points={{-130,196},{-146,196},{-146,174}},
      color={0,170,238},
      thickness=0.5));
  connect(hydraulicCapacitor.portA,Preevaporator. portA_b) annotation (Line(
      points={{-146,166},{-146,80},{-138,80}},
      color={0,170,238},
      thickness=0.5));
  connect(sensor_p.port,sensor_m_flow1. portB) annotation (Line(
      points={{-52,118},{-102.125,118},{-102.125,139},{-86.25,139}},
      color={0,170,238},
      thickness=0.5));
  connect(sensor_T_degC1.port,Preevaporator. portB_a) annotation (Line(
      points={{-38,88},{-38,64},{-110,64}},
      color={153,204,0},
      thickness=0.5));
  connect(moistAirSource.port,TestCoil. portA_gas) annotation (Line(
      points={{110,162},{110,78}},
      color={255,153,0},
      thickness=0.5));
  connect(moistAirSink.port,TestCoil. portB_gas) annotation (Line(
      points={{110,-8},{110,50}},
      color={255,153,0},
      thickness=0.5));
  connect(sensor_T_degC2.port,TestCoil. portA_vle) annotation (Line(
      points={{48,94},{48,64},{96,64}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow2.portB, TestCoil.portA_vle) annotation (Line(
      points={{-8,64},{96,64}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p1.port,TestCoil. portA_vle) annotation (Line(
      points={{8,92},{8,64},{96,64}},
      color={153,204,0},
      thickness=0.5));
  connect(TestCoil.portB_vle,sensor_m_flow3. portA) annotation (Line(
      points={{124,64},{220.5,64}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC3.port,sensor_m_flow3. portA) annotation (Line(
      points={{161,90},{146,90},{146,64},{220.5,64}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q1.port,sensor_m_flow3. portA) annotation (Line(
      points={{199,88},{199,64},{220.5,64}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC4.port, TestCoil.portB_gas) annotation (Line(
      points={{153,18},{110,18},{110,50}},
      color={255,153,0},
      thickness=0.5));
  connect(sensor_T_degC7.port,TestCoil. portA_gas) annotation (Line(
      points={{147,136},{110,136},{110,78}},
      color={255,153,0},
      thickness=0.5));
  connect(sensor_T_degC10.port, CascadeCondenser.portA_a) annotation (Line(
      points={{-4,-198},{60,-198},{60,-128},{72,-128}},
      color={153,204,0},
      thickness=0.5));
  connect(CascadeCondenser.portB_b,pressureState4. portA) annotation (Line(
      points={{100,-112},{198,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p3.port,CascadeCondenser. portA_b) annotation (Line(
      points={{-14,-68},{-14,-112},{72,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC9.port,CascadeCondenser. portA_b) annotation (Line(
      points={{58,-68},{58,-112},{72,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC8.port,pressureState4. portA) annotation (Line(
      points={{110,-66},{110,-112},{198,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p4.port,pressureState4. portA) annotation (Line(
      points={{188,-66},{188,-112},{198,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow5.portA,CascadeCondenser. portA_b) annotation (Line(
      points={{-22.5,-112},{72,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(CascadeCondenser.portB_a,sensor_m_flow7. portA) annotation (Line(
      points={{100,-128},{116,-128},{116,-252},{147,-252}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow7.portB,boundaryUnderdetermined2. port) annotation (Line(
      points={{165,-252},{217,-252}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC11.port, sensor_m_flow7.portA) annotation (Line(
      points={{173,-190},{116,-190},{116,-252},{147,-252}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q2.port, CascadeCondenser.portA_a) annotation (Line(
      points={{-4,-232},{60,-232},{60,-128},{72,-128}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q4.port,CascadeCondenser. portA_b) annotation (Line(
      points={{20,-74},{20,-112},{72,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q5.port,pressureState4. portA) annotation (Line(
      points={{150,-66},{150,-112},{198,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p5.port,sensor_m_flow7. portA) annotation (Line(
      points={{173,-154},{116,-154},{116,-252},{147,-252}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p6.port,CascadeCondenser. portA_a) annotation (Line(
      points={{-5,-166},{60,-166},{60,-128},{72,-128}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow8.portA, RefrigerantPump.portB) annotation (Line(
      points={{-160.5,-112},{-80,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(Preevaporator.portB_a, sensor_m_flow2.portA) annotation (Line(
      points={{-110,64},{-20,64}},
      color={153,204,0},
      thickness=0.5));
  connect(RefrigerantPump.portA, sensor_m_flow5.portB) annotation (Line(
      points={{-60,-112},{-37.5,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow3.portB, pressureState4.portB) annotation (Line(
      points={{235.5,64},{244,64},{244,-112},{228,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q3.port, sensor_m_flow7.portA) annotation (Line(
      points={{171,-224},{116,-224},{116,-252},{147,-252}},
      color={153,204,0},
      thickness=0.5));
  connect(boundaryOverdetermined2.port, sensor_m_flow4.portA) annotation (Line(
      points={{-122,-250},{-75.5,-250}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_m_flow4.portB, pressureState1.portB) annotation (Line(
      points={{-60.5,-250},{-10,-250}},
      color={153,204,0},
      thickness=0.5));
  connect(pressureState1.portA, CascadeCondenser.portA_a) annotation (Line(
      points={{6,-250},{60,-250},{60,-128},{72,-128}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_p7.port, RefrigerantPump.portB) annotation (Line(
      points={{-139,-68},{-139,-112},{-80,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC12.port, RefrigerantPump.portB) annotation (Line(
      points={{-96,-70},{-96,-112},{-80,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_q.port, TestCoil.portA_vle) annotation (Line(
      points={{88,94},{88,64},{96,64}},
      color={153,204,0},
      thickness=0.5));
  ref_mass=CascadeCondenser.summary.path_b.mass_vle+Preevaporator.summary.path_a.mass_vle+TestCoil.summary.mass_vle;

  connect(fillingStation.portA, sensor_m_flow8.portB) annotation (Line(
      points={{-214.667,-36},{-214.667,-112},{-175.5,-112}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_T_degC.port, fillingStation.portB) annotation (Line(
      points={{-194,86},{-194,64},{-214.667,64},{-214.667,-2}},
      color={153,204,0},
      thickness=0.5));
  connect(rotatoryBoundary.rotatoryFlange, RefrigerantPump.rotatoryFlange)
    annotation (Line(
      points={{-104,-144},{-70,-144},{-70,-122}},
      color={135,135,135},
      thickness=0.5));
  connect(PI_m_supply.y, rotatoryBoundary.n_in) annotation (Line(points={{
          -112.4,-168},{-122,-168},{-122,-144},{-108,-144}}, color={0,0,127}));
  connect(set_m_supply.y, PI_m_supply.u_s)
    annotation (Line(points={{-79,-168},{-100.4,-168}}, color={0,0,127}));
  connect(sensor_q.sensorValue, PI_q_supply.u_m) annotation (Line(points={{88,
          109.5},{88,138},{38,138},{38,176.2}},
                                              color={0,0,127}));
  connect(Preevaporator.portA_a, fillingStation.portB) annotation (Line(
      points={{-138,64},{-214.667,64},{-214.667,-2}},
      color={153,204,0},
      thickness=0.5));
  connect(setPoint_quality.y, PI_q_supply.u_s)
    annotation (Line(points={{67.4,180},{56,180},{56,182},{43.6,182}},
                                                    color={0,0,127}));
  connect(PI_m_supply.u_m, sensor_m_flow5.sensorValue) annotation (Line(points=
          {{-106,-173.8},{-106,-188},{-30,-188},{-30,-119.5}}, color={0,0,127}));
  connect(min.y, Heater.Q_flow_in) annotation (Line(points={{9,210},{22,210},{
          22,230},{-124,230},{-124,230}}, color={0,0,127}));
  connect(gain.u, sensor_m_flow2.sensorValue) annotation (Line(points={{-27,143},
          {-26,143},{-26,70},{-14,70}}, color={0,0,127}));
  connect(PI_q_supply.y, add.u1) annotation (Line(points={{31.6,182},{-32,182},
          {-32,163.6},{-1.2,163.6}},  color={0,0,127}));
  connect(initial_heat.y, min.u1)
    annotation (Line(points={{-45.4,216},{-14,216}}, color={0,0,127}));
  connect(gain.y, add.u2) annotation (Line(points={{-15.5,143},{-8,143},{-8,
          156.4},{-1.2,156.4}}, color={0,0,127}));
  connect(min.u2, add.y) annotation (Line(points={{-14,204},{-24,204},{-24,194},
          {24,194},{24,160},{12.6,160}}, color={0,0,127}));
  annotation (
    uses(TIL(version="3.7.0"), TILMedia(version="3.7.0"),
      Modelica(version="3.2.2")),
    Diagram(coordinateSystem(extent={{-260,-300},{260,240}}), graphics={
        Text(
          extent={{-128,170},{-96,160}},
          lineColor={28,108,200},
          textString="Water Loop"),
        Text(
          extent={{-120,20},{-48,0}},
          lineColor={49,112,4},
          textString="Refrigerant side"),
        Text(
          extent={{-118,212},{-86,204}},
          lineColor={207,0,0},
          textString="Heater"),
        Text(
          extent={{-66,162},{-38,152}},
          lineColor={28,108,200},
          textString="Water Pump"),
        Text(
          extent={{-90,76},{-60,68}},
          lineColor={28,108,200},
          textString="(Pre-)Evaporator"),
                  Text(
          extent={{110,86},{132,80}},
          lineColor={28,108,200},
          textString="Test Coil")}),
    Icon(coordinateSystem(extent={{-260,-300},{260,240}})),
    experiment(StopTime=4000, __Dymola_Algorithm="Dassl"));
end PRLtestrev3;
